package tests;

import org.hibernate.SessionFactory;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.junit.After;
import org.junit.Before;

public abstract class AbstractHibernateTestSuite {

    protected SessionFactory sessionFactory;
    protected Session session = null;

    @Before
    public void before() {
        this.sessionFactory = new Configuration().configure().buildSessionFactory();
        this.session = sessionFactory.openSession();
    }

    @After
    public void after() {
        session.close();
        sessionFactory.close();
    }

}
