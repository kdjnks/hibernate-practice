package tests.caching;

import com.practice.entity.Company;
import com.practice.entity.Department;
import com.practice.entity.Person;
import org.hibernate.query.Query;
import org.junit.Test;
import tests.AbstractHibernateTestSuite;

import java.util.ArrayList;
import java.util.List;

public class HibernateCache extends AbstractHibernateTestSuite {

    /*
    * First level cahce is enabled by default and is linked to the session object.
    * For the second level cache we need a provider e.g EHCache.
    * @Cache @Cacheable are used with this cache. Also we should mark entities. And collections because they are not cached by default.
    * */

    @Test
    public void firstLevelCache() {
        setupDepartment();
        //Setup of the data to imitate 1 level cache
        session.close();

        session = sessionFactory.openSession();

        Department department = session.load(Department.class, 1L);
        System.out.println(department);

        //Department is in the session cache so it won't be loaded here
        department = session.load(Department.class, 1L);
        System.out.println(department);

    }

    @Test
    public void secondLevelCache() {
        setupCompany(1);

        session.close();

        session = sessionFactory.openSession();

        Company c = session.load(Company.class, 1L);
        System.out.println(c);

        session.close();

        session = sessionFactory.openSession();

        //Department is in the session cache lvl2 so it won't be loaded here
        c = session.load(Company.class, 1L);
        System.out.println(c);
    }

    @Test
    public void queryCache() {
        setupCompany(1);

        session.close();

        session = sessionFactory.openSession();

        Query query = session.createQuery("SELECT c FROM Company c", Company.class);
        query.setCacheable(true);

        List<Company> companyList = query.list();
        companyList.forEach(System.out::println);

        companyList = query.list();
        companyList.forEach(System.out::println);

    }

    private void setupDepartment() {
        session.beginTransaction();

        Department d = new Department("Test Cache Level 1");
        session.persist(d);

        session.getTransaction().commit();
    }

    private void setupCompany(int id) {
        session.beginTransaction();

        Company c = new Company("Test Cache Level 2 /" + id);
        session.persist(c);

        session.getTransaction().commit();
    }

}
