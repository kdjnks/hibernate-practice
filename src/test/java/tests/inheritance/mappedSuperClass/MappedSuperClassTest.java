package tests.inheritance.mappedSuperClass;

import com.practice.entity.mapped.KitchenMouse;
import com.practice.entity.mapped.LibraryMouse;
import org.hibernate.query.Query;
import org.junit.Test;
import tests.AbstractHibernateTestSuite;

public class MappedSuperClassTest extends AbstractHibernateTestSuite {

    @Test
    public void retrieve() {
        session.beginTransaction();
        init();
        Query<LibraryMouse> query = session.createQuery("select m from LibraryMouse m", LibraryMouse.class);
        query.getResultList();
        session.getTransaction().commit();
    }

    private void init() {
        LibraryMouse libraryMouse = new LibraryMouse("libraryMouse", "Harry Potter");
        KitchenMouse kitchenMouse = new KitchenMouse("kitchen", "cheese");
        session.persist(libraryMouse);
        session.persist(kitchenMouse);
    }
}
