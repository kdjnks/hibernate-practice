package tests.inheritance.tablePerClass;

import com.practice.entity.tablePerClass.KitchenMousePerTable;
import com.practice.entity.tablePerClass.LibraryMousePerTable;
import com.practice.entity.tablePerClass.MousePerTable;
import org.junit.Test;
import tests.AbstractHibernateTestSuite;

public class TablePerClassTest extends AbstractHibernateTestSuite {

    @Test
    public void retrieve() {
        session.beginTransaction();
        init();
        session.createQuery("select m from MousePerTable m", MousePerTable.class).getResultList().forEach(System.out::println);

        session.getTransaction().commit();
    }

    private void init() {
        LibraryMousePerTable libraryMousePerTable = new LibraryMousePerTable("library mouse", "book");
        session.persist(libraryMousePerTable);

        KitchenMousePerTable kitchenMousePerTable = new KitchenMousePerTable("kitchen mouse", "kitchen");
        session.persist(kitchenMousePerTable);
    }
}
