package tests.inheritance.singleTable;

import com.practice.entity.singleTable.KitchenMouseSingleTable;
import com.practice.entity.singleTable.LibraryMouseSingleTable;
import com.practice.entity.singleTable.MouseSingleTable;
import org.junit.Test;
import tests.AbstractHibernateTestSuite;

public class SingleTableTest extends AbstractHibernateTestSuite {

    @Test
    public void retrieve() {
        session.beginTransaction();
        init();
        session.createQuery("select m from MouseSingleTable m", MouseSingleTable.class).getResultList();

        session.getTransaction().commit();
    }

    private void init() {
        LibraryMouseSingleTable libraryMouse = new LibraryMouseSingleTable("libraryMouse", "Harry Potter");
        KitchenMouseSingleTable kitchenMouse = new KitchenMouseSingleTable("kitchen", "cheese");
        session.persist(libraryMouse);
        session.persist(kitchenMouse);
    }
}
