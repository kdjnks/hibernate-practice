package tests.inheritance.joined;

import com.practice.entity.joined.KitchenMouseJoined;
import com.practice.entity.joined.LibraryMouseJoined;
import org.junit.Test;
import tests.AbstractHibernateTestSuite;

public class JoinedTest extends AbstractHibernateTestSuite {

    @Test
    public void retrieve() {
        session.beginTransaction();
        init();
        session.createQuery("select m from KitchenMouseJoined m", KitchenMouseJoined.class).getResultList();
        session.getTransaction().commit();
    }

    private void init() {
        KitchenMouseJoined kitchenMouseJoined = new KitchenMouseJoined("kitchen mouse", "cheese");
        LibraryMouseJoined libraryMouseJoined = new LibraryMouseJoined("library mouse", "book");
        session.persist(kitchenMouseJoined);
        session.persist(libraryMouseJoined);
    }
}
