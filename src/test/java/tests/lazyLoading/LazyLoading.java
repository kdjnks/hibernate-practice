package tests.lazyLoading;

import com.practice.entity.Department;
import com.practice.entity.Employee;
import org.junit.Test;
import tests.AbstractHibernateTestSuite;

import java.util.Arrays;

public class LazyLoading extends AbstractHibernateTestSuite {

    @Override
    public void before() {
        super.before();
        setupDepartmnents();
    }

    @Test
    public void lazyLoading() {
        setupDepartmnents();
        session.close();

        session = sessionFactory.openSession();

        //HQL is a bit different with fetch.join strategy..
        //Department department = (Department) session.createQuery("FROM Department d where d.id = 1").getSingleResult();
        Department department = session.find(Department.class, 1L);
        //Department department = (Department) session.createQuery("FROM Department d JOIN FETCH d.employees where d.id = 1").getSingleResult();

    }

    private void setupDepartmnents() {
        session.beginTransaction();

        Department itDepartment = new Department();
        itDepartment.setName("IT Department");

        Department hrDepartment = new Department();
        hrDepartment.setName("HR Department");

        Employee employee = new Employee();
        employee.setName("Test_Employee_1");

        hrDepartment.setEmployees(Arrays.asList(employee));

        session.save(employee);
        session.save(hrDepartment);
        session.save(itDepartment);
        session.getTransaction().commit();
    }
}
