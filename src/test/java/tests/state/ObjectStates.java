package tests.state;

import com.practice.entity.Person;
import org.junit.Test;
import tests.AbstractHibernateTestSuite;

public class ObjectStates extends AbstractHibernateTestSuite {

    @Test
    public void objectStates() {
        //transient
        Person p = new Person();
        p.setName("Name");
        p.setAge(11);

        Person p2 = new Person();
        p2.setName("Name2");
        p2.setAge(112);

        session.beginTransaction();

        //Persistent state
        session.persist(p);
        session.persist(p2);

        //In the removed state(after the session is closed it will be cleared by garbage collector)
        session.remove(p2);

        //Making a detached object. If we use clear() it will do evict() for all session objects.
        session.evict(p);

        session.getTransaction().commit();

        //After the session closed, object is in a detached state - not managed by persistent context anymore.
        session.close();



    }
}
