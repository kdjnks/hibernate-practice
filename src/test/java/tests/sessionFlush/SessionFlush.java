package tests.sessionFlush;

import com.practice.entity.Department;
import com.practice.entity.Employee;
import org.hibernate.Transaction;
import org.junit.Test;
import tests.AbstractHibernateTestSuite;

import java.util.Arrays;

public class SessionFlush extends AbstractHibernateTestSuite {

    @Test
    public void sessionFlush() {
        //session.setFlushMode(FlushModeType.MANUAL);

        Department d1 = new Department("Department1");
        Department d2 = new Department("Department2");

        Transaction tx = session.beginTransaction();

        session.save(d1);
        session.save(d2);

        Employee e1 = new Employee("Employee1");

        session.save(e1);

        d1.setEmployees(Arrays.asList(e1));
        d2.setEmployees(Arrays.asList(e1));

        //Dirty session is not dirty anymore? is session.flush(..)  executed?
        session.createQuery("select employees from Department ");


    }

}
