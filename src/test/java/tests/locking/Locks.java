package tests.locking;

import com.practice.entity.Document;
import org.hibernate.query.Query;
import org.junit.Test;
import tests.AbstractHibernateTestSuite;

import javax.persistence.LockModeType;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

//FIXME Needs a complete refactor.
public class Locks extends AbstractHibernateTestSuite {

    private ExecutorService executorService;

    @Override
    public void before() {
        super.before();

        this.executorService = Executors.newFixedThreadPool(2);
    }

    @Test
    public void optimisticLock() throws ExecutionException, InterruptedException {
        setupDocument();

        executorService.execute( () -> {
            renameDocument(2, 10000);
            try {
                sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        executorService.execute( () -> {
            try {
                sleep(30000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            renameDocument(3, 10000);

        });

        awaitTerminationAfterShutdown(executorService);

    }

    @Test
    public void pessimisticLock() {
        setupDocument();

        executorService.execute( () -> {
            renameDocument(2, 10000);
        });

        executorService.execute( () -> {
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            renameDocument(3, 10000);

        });

        awaitTerminationAfterShutdown(executorService);
    }

    private void renameDocument(int id, long sleep) {
        session.beginTransaction();

        Query<Document> q = session.createQuery("SELECT d FROM Document d", Document.class);
        q.setLockMode(LockModeType.OPTIMISTIC_FORCE_INCREMENT);
        Document d = q.getSingleResult();

        try {
            sleep(sleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        d.setName("Document " + id);

        session.update(d);

        System.out.println(String.format("Document with name %s was updated and the version is %d", d.getName(), d.getVersion()));

        session.getTransaction().commit();
    }

    private void setupDocument() {
        session.beginTransaction();

        Document c = new Document("Document 1");
        session.persist(c);

        System.out.println("Document was saved.");

        session.getTransaction().commit();
    }

    private void awaitTerminationAfterShutdown(ExecutorService threadPool) {
        threadPool.shutdown();
        try {
            if (!threadPool.awaitTermination(60, TimeUnit.SECONDS)) {
                threadPool.shutdownNow();
            }
        } catch (InterruptedException ex) {
            threadPool.shutdownNow();
            Thread.currentThread().interrupt();
        }
    }
}
