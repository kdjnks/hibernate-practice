package tests.orphanRemoval;

import com.practice.entity.Department;
import com.practice.entity.Employee;
import org.junit.Test;
import tests.AbstractHibernateTestSuite;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

public class OrphanRemoval extends AbstractHibernateTestSuite {

    @Test
    public void orphanRemoval() {
        setupDepartment();
        session.close();

        session = sessionFactory.openSession();
        session.getTransaction().begin();

        Department department = session.find(Department.class, 1L);
        session.remove(department);

        session.getTransaction().commit();

        department = session.find(Department.class, 1L);
        Employee employee = session.find(Employee.class, 1L);

        assertEquals(null, department);
        assertEquals(null, employee);

    }

    private void setupDepartment() {
        session.beginTransaction();
        Department testDepartment = new Department("TestDepartment");
        Employee employee = new Employee("Test Employee 1");

        testDepartment.setEmployees(Arrays.asList(employee));

        session.persist(employee);
        session.persist(testDepartment);

        session.getTransaction().commit();

    }
}
