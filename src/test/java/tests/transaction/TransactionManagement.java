package tests.transaction;

import com.practice.entity.Person;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.Test;
import tests.AbstractHibernateTestSuite;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TransactionManagement extends AbstractHibernateTestSuite {

    @Test
    public void transactionIsSuccessful() {
        Transaction transaction = session.beginTransaction();

        try {
            Person p = new Person();
            p.setName("Name");
            p.setAge(11);

            session.persist(p);

            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        }

        Query query = session.createQuery("SELECT p FROM Person p", Person.class);
        List<Person> persons = query.getResultList();

        assertEquals(1, persons.size());
    }

}
