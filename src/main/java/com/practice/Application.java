package com.practice;

import com.practice.entity.Department;
import com.practice.entity.Employee;
import com.practice.entity.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.Arrays;
import java.util.List;

public class Application {

    private SessionFactory sessionFactory;

    public Application() {
        this.sessionFactory = new Configuration().configure().buildSessionFactory();

        businessLogic();

        sessionFactory.close();
    }

    private void businessLogic() {

        objectStates();
        //lazyLoadingAndNProblem();
        //transactionManagement();
    }

    private void objectStates() {
        //transient (new)
        Person test = new Person();
        test.setName("Test");
        test.setAge(23);

        Person test2 = new Person();
        test2.setName("Test2");
        test2.setAge(24);

        Session session = sessionFactory.openSession();

        session.beginTransaction();

        //Now it is in the persistent context(Persistent state)
        session.save(test);
        session.save(test2);

        session.remove(test);

        session.getTransaction().commit();

        List<Person> persons = session.createQuery("SELECT p FROM Person p", Person.class).getResultList();

        System.out.println("------------");
        persons.forEach(System.out::println);

        //After the session is closed it's in a detached state.
        session.close();
    }

    public void lazyLoadingAndNProblem() {
        Session session = sessionFactory.openSession();

        session.beginTransaction();

        Department itDepartment = new Department();
        itDepartment.setName("IT Department");

        Department hrDepartment = new Department();
        hrDepartment.setName("HR Department");

        Employee employee = new Employee();
        employee.setName("Test_Employee_1");

        hrDepartment.setEmployees(Arrays.asList(employee));

        session.save(employee);
        session.save(hrDepartment);
        session.save(itDepartment);
        session.getTransaction().commit();
        session.close();

        //New Session

        session = sessionFactory.openSession();

        Department department = session.find(Department.class, 1L);

        System.out.println(department.getEmployees());

        session.close();
    }

    public void transactionManagement() {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();

        Query query = session.createQuery("SELECT p FROM Person p", Person.class);
        List<Person> persons = query.getResultList();

        try {
            Person p = new Person();
            p.setName("Name");
            p.setAge(11);

            session.persist(p);
            //doSomething();

            transaction.commit();
        } catch (Exception e) {
            transaction.rollback();
        }

        persons = query.getResultList();

        persons.forEach(System.out::println);
    }

    private void doSomething() throws Exception {
        throw new Exception("doSomething");
    }

    public static void main(String[] args) {
        new Application();
    }

}