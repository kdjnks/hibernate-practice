package com.practice.entity.tablePerClass;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "kitchen_mouse_per_table")
public class KitchenMousePerTable extends MousePerTable {

    private String favouriteCheese;

    public KitchenMousePerTable(String name, String favouriteCheese) {
        super(name);
        this.favouriteCheese = favouriteCheese;
    }

    public KitchenMousePerTable() {
    }

    public String getFavouriteCheese() {
        return favouriteCheese;
    }
}
