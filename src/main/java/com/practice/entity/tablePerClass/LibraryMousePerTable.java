package com.practice.entity.tablePerClass;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "library_mouse_per_table")
public class LibraryMousePerTable extends MousePerTable {

    private String favouriteBook;

    public LibraryMousePerTable(String name, String favouriteBook) {
        super(name);
        this.favouriteBook = favouriteBook;
    }

    public LibraryMousePerTable() {
    }

    public String getFavouriteBook() {
        return favouriteBook;
    }
}
