package com.practice.entity.tablePerClass;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class MousePerTable {

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    private Long id;

    private String name;

    public MousePerTable(String name) {
        this.name = name;
    }

    public MousePerTable() {
    }

    public String getName() {
        return name;
    }
}
