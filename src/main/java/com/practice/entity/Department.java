package com.practice.entity;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "departments")
public class Department {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "employee_id")
    @Fetch(FetchMode.JOIN) //SELECT, SUBSELECT, JOIN
    //
    //@BatchSize(size = 5)
    private List<Employee> employees;

    /**
     * Select is chosen by default and generates n+1 problem.
     * Join is only applied to session.find(..) like queries.
    * */

    /**
     * SELECT/SUBSELECT for HQL and Criteria are the same.
     *
     * SELECT FetchMode
     * Hibernate:
     *     select
     *         department0_.id as id1_0_0_,
     *         department0_.name as name2_0_0_
     *     from
     *         departments department0_
     *     where
     *         department0_.id=?
     * Hibernate:
     *     select
     *         employees0_.employee_id as employee3_1_0_,
     *         employees0_.id as id1_1_0_,
     *         employees0_.id as id1_1_1_,
     *         employees0_.name as name2_1_1_
     *     from
     *         employees employees0_
     *     where
     *         employees0_.employee_id=?
     *
     * SUBSELECT FetchMode
     * Hibernate:
     *     select
     *         department0_.id as id1_0_0_,
     *         department0_.name as name2_0_0_
     *     from
     *         departments department0_
     *     where
     *         department0_.id=?
     * Hibernate:
     *     select
     *         employees0_.employee_id as employee3_1_0_,
     *         employees0_.id as id1_1_0_,
     *         employees0_.id as id1_1_1_,
     *         employees0_.name as name2_1_1_
     *     from
     *         employees employees0_
     *     where
     *         employees0_.employee_id=?
     *
     *
     * JOIN FetchMode
     * Hibernate:
     *     select
     *         department0_.id as id1_0_0_,
     *         department0_.name as name2_0_0_,
     *         employees1_.employee_id as employee3_1_1_,
     *         employees1_.id as id1_1_1_,
     *         employees1_.id as id1_1_2_,
     *         employees1_.name as name2_1_2_
     *     from
     *         departments department0_
     *     left outer join
     *         employees employees1_
     *             on department0_.id=employees1_.employee_id
     *     where
     *         department0_.id=?
     * */

    public Department() {
    }

    public Department(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    @Override
    public String toString() {
        return "Department{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", employees=" + employees +
                '}';
    }
}
