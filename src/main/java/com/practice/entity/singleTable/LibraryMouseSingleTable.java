package com.practice.entity.singleTable;

import javax.persistence.*;

@Entity
@Table(name = "library_mouse_single_table")
@DiscriminatorValue(value = "LM")
public class LibraryMouseSingleTable extends MouseSingleTable {

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    private Long id;

    private String favouriteBook;

    public LibraryMouseSingleTable(String name, String favouriteBook) {
        super(name);
        this.favouriteBook = favouriteBook;
    }

    public LibraryMouseSingleTable() {
    }

    public String getFavouriteBook() {
        return favouriteBook;
    }
}
