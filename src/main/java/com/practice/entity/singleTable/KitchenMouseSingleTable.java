package com.practice.entity.singleTable;

import javax.persistence.*;

@Entity
@Table(name = "kitchen_mouse_single_table")
@DiscriminatorValue(value = "KM")
public class KitchenMouseSingleTable extends MouseSingleTable {

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    private Long id;

    private String favouriteCheese;

    public KitchenMouseSingleTable(String name, String favouriteCheese) {
        super(name);
        this.favouriteCheese = favouriteCheese;
    }

    public KitchenMouseSingleTable() {
    }

    public String getFavouriteCheese() {
        return favouriteCheese;
    }
}
