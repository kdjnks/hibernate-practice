package com.practice.entity.singleTable;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "mouse_type")
public abstract class MouseSingleTable {

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    private Long id;

    private String name;

    public MouseSingleTable(String name) {
        this.name = name;
    }

    public MouseSingleTable() {
    }

    public String getName() {
        return name;
    }
}
