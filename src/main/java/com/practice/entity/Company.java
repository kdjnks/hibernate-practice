package com.practice.entity;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

@Entity
@Table(name = "company")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
/*
* When to use Cache strategies
* READ_ONLY - If entity is never changed. (Static data)
* NONSTRICT_READ_WRITE - Transaction made some changes to the cacheable data and there is a small time gap when old cache can be retrieved
*
* */
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    public Company() {
    }

    public Company(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Company{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
