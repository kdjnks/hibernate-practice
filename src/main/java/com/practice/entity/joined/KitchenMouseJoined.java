package com.practice.entity.joined;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "kitchen_mouse_joined")
public class KitchenMouseJoined extends MouseJoined {

    private String favouriteCheese;

    public KitchenMouseJoined(String name, String favouriteCheese) {
        super(name);
        this.favouriteCheese = favouriteCheese;
    }

    public KitchenMouseJoined() {
    }

    public String getFavouriteCheese() {
        return favouriteCheese;
    }
}
