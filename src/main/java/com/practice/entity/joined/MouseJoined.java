package com.practice.entity.joined;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class MouseJoined {

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    private Long id;

    private String name;

    public MouseJoined(String name) {
        this.name = name;
    }

    public MouseJoined() {
    }

    public String getName() {
        return name;
    }
}
