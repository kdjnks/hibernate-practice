package com.practice.entity.joined;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "library_mouse_joined")
public class LibraryMouseJoined extends MouseJoined {

    private String favouriteBook;

    public LibraryMouseJoined(String name, String favouriteBook) {
        super(name);
        this.favouriteBook = favouriteBook;
    }

    public LibraryMouseJoined() {
    }

    public String getFavouriteBook() {
        return favouriteBook;
    }
}
