package com.practice.entity.mapped;

import javax.persistence.*;

@Entity
@Table(name = "library_mouse")
public class LibraryMouse extends Mouse {

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    private Long id;

    private String favouriteBook;

    public LibraryMouse(String name, String favouriteBook) {
        super(name);
        this.favouriteBook = favouriteBook;
    }

    public LibraryMouse() {
    }

    public String getFavouriteBook() {
        return favouriteBook;
    }
}
