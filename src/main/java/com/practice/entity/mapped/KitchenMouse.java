package com.practice.entity.mapped;

import javax.persistence.*;

@Entity
@Table(name = "kitchen_mouse")
public class KitchenMouse extends Mouse {

    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Id
    private Long id;

    private String favouriteCheese;

    public KitchenMouse(String name, String favouriteCheese) {
        super(name);
        this.favouriteCheese = favouriteCheese;
    }

    public KitchenMouse() {
    }

    public String getFavouriteCheese() {
        return favouriteCheese;
    }
}
