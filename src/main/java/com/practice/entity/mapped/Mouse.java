package com.practice.entity.mapped;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class Mouse {

    private String name;

    public Mouse(String name) {
        this.name = name;
    }

    public Mouse() {
    }

    public String getName() {
        return name;
    }
}
